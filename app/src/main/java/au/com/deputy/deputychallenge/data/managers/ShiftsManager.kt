package au.com.deputy.deputychallenge.data.managers

import android.support.annotation.NonNull
import au.com.deputy.deputychallenge.data.api.entities.ShiftDetails
import au.com.deputy.deputychallenge.data.api.entities.ShiftListResponse
import au.com.deputy.deputychallenge.data.api.services.ShiftsService
import au.com.deputy.deputychallenge.data.mappers.ShiftsListResponseMapper
import au.com.deputy.deputychallenge.data.repositories.ShiftsHandlerCallback
import au.com.deputy.deputychallenge.data.repositories.ShiftsRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class ShiftsManager @Inject constructor(private val shiftsService: ShiftsService) : ShiftsRepository {

    override fun getShifts(@NonNull callbackListener: ShiftsHandlerCallback) {
        val shiftsCall = shiftsService.getShifts()
        shiftsCall.enqueue(object : Callback<List<ShiftListResponse>?> {
            override fun onFailure(call: Call<List<ShiftListResponse>?>?, t: Throwable?) {
                callbackListener.onShiftsLoadedFailure()
            }

            override fun onResponse(call: Call<List<ShiftListResponse>?>, response: Response<List<ShiftListResponse>?>) {
                if (response.isSuccessful) {
                    callbackListener.onShiftsLoaded(ShiftsListResponseMapper.maptoShiftList(response.body()))
                } else {
                    callbackListener.onShiftsLoadedFailure()
                }
            }
        })
    }

    override fun toggleShift(isStartingShift: Boolean, shift: ShiftDetails, @NonNull callbackListener: ShiftsHandlerCallback) {
        var shiftsCall: Call<Void>
        if (isStartingShift) {
            shiftsCall = shiftsService.startShift(shift)
        } else {
            shiftsCall = shiftsService.endShift(shift)
        }
        shiftsCall.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                callbackListener.onShiftToggleResult(true)
            }

            override fun onFailure(call: Call<Void>?, t: Throwable?) {
                callbackListener.onShiftToggleResult(false)
            }
        })
    }
}