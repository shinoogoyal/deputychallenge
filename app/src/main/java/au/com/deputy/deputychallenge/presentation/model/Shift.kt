package au.com.deputy.deputychallenge.presentation.model

class Shift (val time: String,
             val latitude:String,
             val longitude: String)