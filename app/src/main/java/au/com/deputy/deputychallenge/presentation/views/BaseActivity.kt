package au.com.deputy.deputychallenge.presentation.views

import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import au.com.deputy.deputychallenge.ShiftsApplication
import au.com.deputy.deputychallenge.injection.components.DaggerActivityComponent

abstract class BaseActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        injectActivity()
    }

    private fun getAppComponent() = (applicationContext as ShiftsApplication).applicationComponent

    fun getAppInjector() = DaggerActivityComponent.builder().applicationComponent(getAppComponent()).build()

    abstract fun injectActivity()
}