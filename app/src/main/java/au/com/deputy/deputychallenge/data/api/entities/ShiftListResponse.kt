package au.com.deputy.deputychallenge.data.api.entities

import com.google.gson.annotations.SerializedName

data class ShiftListResponse(@SerializedName("id") val id: Int,
                                @SerializedName("start") val start: String,
                                @SerializedName("end") val end: String,
                                @SerializedName("startLatitude") val startLatitude: String,
                                @SerializedName("endLatitude") val endLatitude: String,
                                @SerializedName("startLongitude") val startLongitude: String,
                                @SerializedName("endLongitude") val endLongitude: String,
                                @SerializedName("image") val image: String)