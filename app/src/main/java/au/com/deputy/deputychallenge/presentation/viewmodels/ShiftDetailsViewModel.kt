package au.com.deputy.deputychallenge.presentation.viewmodels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import au.com.deputy.deputychallenge.data.api.entities.PreviousShiftListDetails
import au.com.deputy.deputychallenge.data.api.entities.ShiftDetails
import au.com.deputy.deputychallenge.data.repositories.ShiftsHandlerCallback
import au.com.deputy.deputychallenge.data.repositories.ShiftsRepository
import au.com.deputy.deputychallenge.injection.components.DaggerViewModelComponent
import au.com.deputy.deputychallenge.presentation.model.PreviousShift
import au.com.deputy.deputychallenge.presentation.model.PreviousShiftList
import au.com.deputy.deputychallenge.presentation.model.Shift
import javax.inject.Inject

class ShiftDetailsViewModel() : ViewModel(), ShiftsHandlerCallback {

    @Inject
    lateinit var shiftsRepository: ShiftsRepository

    val shiftsList = MutableLiveData<au.com.deputy.deputychallenge.presentation.model.PreviousShiftList>()
    val shiftToggleState = MutableLiveData<Boolean>()
    val errorData = MutableLiveData<Exception>()

    init {
        inject()
    }

    fun inject() = DaggerViewModelComponent.builder().build().inject(this)

    fun getShiftsList() = shiftsRepository.getShifts(this)

    fun handleShift(isShiftStarting: Boolean, shift: Shift) {
        shiftsRepository.toggleShift(isShiftStarting, ShiftDetails(shift.time, shift.latitude, shift.longitude), this)
    }

    override fun onShiftsLoaded(shifts: PreviousShiftListDetails) {
        val listOfShifts = mutableListOf<PreviousShift>()

        shifts.shifts.reversed().forEach {
            listOfShifts.add(PreviousShift(it.id, it.start, it.end, it.image))
        }
        shiftsList.postValue(PreviousShiftList(listOfShifts))
    }

    override fun onShiftToggleResult(result: Boolean) = shiftToggleState.postValue(result)

    override fun onShiftsLoadedFailure() {
        errorData.postValue(java.lang.Exception())
    }
}