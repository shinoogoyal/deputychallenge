package au.com.deputy.deputychallenge.data.api.services

import au.com.deputy.deputychallenge.data.api.entities.ShiftDetails
import au.com.deputy.deputychallenge.data.api.entities.ShiftListResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST

interface ShiftsService {
    @Headers("Authorization: Deputy cf6c0ad5bd1d0744a8636b8bc0bdc7bb3d7d0c81")
    @POST("shift/start")
    fun startShift(@Body shiftDetails: ShiftDetails): Call<Void>

    @Headers("Authorization: Deputy cf6c0ad5bd1d0744a8636b8bc0bdc7bb3d7d0c81")
    @POST("shift/end")
    fun endShift(@Body shiftDetails: ShiftDetails): Call<Void>

    @Headers("Authorization: Deputy cf6c0ad5bd1d0744a8636b8bc0bdc7bb3d7d0c81")
    @GET("shifts")
    fun getShifts(): Call<List<ShiftListResponse>?>
}