package au.com.deputy.deputychallenge.data.mappers

import au.com.deputy.deputychallenge.data.api.entities.PreviousShift
import au.com.deputy.deputychallenge.data.api.entities.PreviousShiftListDetails
import au.com.deputy.deputychallenge.data.api.entities.ShiftListResponse

class ShiftsListResponseMapper {
    companion object {
        fun maptoShiftList(shiftListResponse: List<ShiftListResponse>?): PreviousShiftListDetails {
            var shiftsList = mutableListOf<PreviousShift>()
            shiftListResponse?.let {
                it.forEach {
                    shiftsList.add(PreviousShift(it.id.toString(), it.start, it.end, it.startLatitude, it.endLatitude, it.startLongitude, it.endLongitude, it.image))
                }
            }
            return PreviousShiftListDetails(shiftsList)
        }
    }
}