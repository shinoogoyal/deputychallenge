package au.com.deputy.deputychallenge.presentation.util

import java.text.SimpleDateFormat
import java.util.*

fun Date.toDDMMMMYYYYHHMMSS() = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(this)