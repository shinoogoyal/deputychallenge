package au.com.deputy.deputychallenge.presentation.model

class PreviousShiftList(val shifts: List<PreviousShift>)

class PreviousShift(val id: String,
                    val start: String,
                    val end: String,
                    val image: String)