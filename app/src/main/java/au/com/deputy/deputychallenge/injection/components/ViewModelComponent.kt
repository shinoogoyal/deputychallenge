package au.com.deputy.deputychallenge.injection.components;

import au.com.deputy.deputychallenge.injection.modules.NetworkModule
import au.com.deputy.deputychallenge.injection.modules.RepositoryModule
import au.com.deputy.deputychallenge.presentation.viewmodels.ShiftDetailsViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [RepositoryModule::class, NetworkModule::class])
interface ViewModelComponent {
    fun inject(shiftDetailsViewModel: ShiftDetailsViewModel)
}