package au.com.deputy.deputychallenge.injection.components;

import android.content.Context
import au.com.deputy.deputychallenge.ShiftsApplication
import au.com.deputy.deputychallenge.injection.modules.ApplicationModule
import au.com.deputy.deputychallenge.injection.modules.NetworkModule
import au.com.deputy.deputychallenge.injection.modules.RepositoryModule
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [ApplicationModule::class, RepositoryModule::class, NetworkModule::class])
interface ApplicationComponent {
    fun provideContext(): Context
    fun inject(application: ShiftsApplication)
}
