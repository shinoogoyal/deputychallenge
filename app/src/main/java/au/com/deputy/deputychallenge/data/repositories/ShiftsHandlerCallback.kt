package au.com.deputy.deputychallenge.data.repositories

import au.com.deputy.deputychallenge.data.api.entities.PreviousShiftListDetails

interface ShiftsHandlerCallback {
    fun onShiftsLoaded(shifts: PreviousShiftListDetails)
    fun onShiftsLoadedFailure()
    fun onShiftToggleResult(result: Boolean)
}