package au.com.deputy.deputychallenge.data.repositories

import android.support.annotation.NonNull
import au.com.deputy.deputychallenge.data.api.entities.ShiftDetails

interface ShiftsRepository {

    fun getShifts(@NonNull callbackListener: ShiftsHandlerCallback)
    fun toggleShift(isStartingShift: Boolean, shift: ShiftDetails, @NonNull callbackListener: ShiftsHandlerCallback)
}