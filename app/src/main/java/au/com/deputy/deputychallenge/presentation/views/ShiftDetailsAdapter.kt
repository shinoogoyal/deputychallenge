package au.com.deputy.deputychallenge.presentation.views

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import au.com.deputy.deputychallenge.R
import au.com.deputy.deputychallenge.presentation.model.PreviousShift
import au.com.deputy.deputychallenge.presentation.model.PreviousShiftList
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.layout_shift_item.view.*

class ShiftDetailsAdapter() : RecyclerView.Adapter<ShiftDetailsAdapter.ShiftDetailsItemViewHolder>() {

    private var shiftsList: PreviousShiftList = PreviousShiftList(emptyList())

    override fun getItemCount() = shiftsList.shifts.size

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ShiftDetailsItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_shift_item, parent, false)
        return ShiftDetailsItemViewHolder(view)
    }

    fun setShiftsList(list: PreviousShiftList) {
        this.shiftsList = list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ShiftDetailsItemViewHolder, position: Int) {
        holder.bindData(shiftsList.shifts[position])
    }

    class ShiftDetailsItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(shift: PreviousShift) {

            itemView.lbl_end_time.visibility = View.VISIBLE
            itemView.txt_end_time.visibility = View.VISIBLE

            itemView.txt_start_time.text = shift.start
            shift.end.let {
                if (it.isNotBlank()) {
                    itemView.txt_end_time.text = it
                } else {
                    itemView.lbl_end_time.visibility = View.GONE
                    itemView.txt_end_time.visibility = View.GONE
                }
            }

            Picasso.with(itemView.context).load(shift.image).into(itemView.img_shift)
        }
    }
}

