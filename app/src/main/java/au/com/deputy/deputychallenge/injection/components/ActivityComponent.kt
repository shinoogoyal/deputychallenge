package au.com.deputy.deputychallenge.injection.components;

import au.com.deputy.deputychallenge.injection.annotations.PerScreen
import au.com.deputy.deputychallenge.presentation.views.ShiftDetailsActivity
import dagger.Component

@PerScreen
@Component(dependencies = [ApplicationComponent::class])
interface ActivityComponent {
    fun inject(activity: ShiftDetailsActivity)
}