package au.com.deputy.deputychallenge

import android.app.Application
import au.com.deputy.deputychallenge.injection.components.ApplicationComponent
import au.com.deputy.deputychallenge.injection.components.DaggerApplicationComponent
import au.com.deputy.deputychallenge.injection.modules.ApplicationModule
import javax.inject.Inject

class ShiftsApplication : Application() {

    @Inject
    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        inject()
    }

    fun inject() {
        DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build().inject(this)
    }
}