package au.com.deputy.deputychallenge.presentation.views

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import au.com.deputy.deputychallenge.R
import au.com.deputy.deputychallenge.presentation.model.Shift
import au.com.deputy.deputychallenge.presentation.util.toDDMMMMYYYYHHMMSS
import au.com.deputy.deputychallenge.presentation.viewmodels.ShiftDetailsViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import kotlinx.android.synthetic.main.activity_shift_details.*
import java.util.*


class ShiftDetailsActivity : BaseActivity() {

    private val TAG: String = "ShiftDetailsActivity"
    private val REQUEST_CODE: Int = 1
    val IS_SHIFT_STARTED: String = "is_shift_started"

    private val listAdapter = ShiftDetailsAdapter()
    private lateinit var viewModel: ShiftDetailsViewModel
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var lastLatitude: String = "0.00"
    private var lastLongitude: String = "0.00"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var isShiftStarted = false
        savedInstanceState?.let { isShiftStarted = it.getBoolean(IS_SHIFT_STARTED, false) }
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        injectActivity()
        setContentView(R.layout.activity_shift_details)
        setupObservers()
        setUpListeners(isShiftStarted)
        viewModel.getShiftsList()
    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        super.onSaveInstanceState(outState, outPersistentState)
        outState?.let { it.putBoolean(IS_SHIFT_STARTED, isShiftStarted()) }
    }

    override fun onResume() {
        super.onResume()
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            setUpLocationListeners()
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE)
        }
    }

    override fun injectActivity() = getAppInjector().inject(this)

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setUpLocationListeners()
            }
        }
    }

    private fun setUpListeners(isShiftStarted: Boolean) {
        if (isShiftStarted) btn_shift.text = getString(R.string.end_shift)

        btn_shift.setOnClickListener {
            btn_shift.isEnabled = false
            viewModel.handleShift(!isShiftStarted(),
                    Shift(Date().toDDMMMMYYYYHHMMSS().toString(), lastLatitude, lastLatitude))
        }
    }

    private fun setupObservers() {
        viewModel = ViewModelProviders.of(this).get(ShiftDetailsViewModel::class.java)
        viewModel.shiftsList.observe(this, android.arch.lifecycle.Observer {
            it?.let { listAdapter.setShiftsList(it) }
        })

        viewModel.errorData.observe(this, Observer {
            handleError()
        })

        viewModel.shiftToggleState.observe(this, Observer {
            it?.let {
                if (it) {
                    toggleButtonText()
                    //refresh list of shifts
                    viewModel.getShiftsList()
                }
            }

            btn_shift.isEnabled = true
        })

        val layoutManager = LinearLayoutManager(this)
        val dividerItemDecoration = DividerItemDecoration(list_shifts.context, layoutManager.orientation)
        list_shifts.addItemDecoration(dividerItemDecoration)
        list_shifts.layoutManager = layoutManager
        list_shifts.adapter = listAdapter
    }

    private fun setUpLocationListeners() {
        fusedLocationClient.lastLocation
                .addOnSuccessListener { location: Location? ->
                    location?.let {
                        lastLatitude = it.latitude.toString()
                        lastLongitude = it.longitude.toString()
                    }
                }
    }

    private fun toggleButtonText() {
        if (btn_shift.text == getString(R.string.start_shift))
            btn_shift.text = getString(R.string.end_shift)
        else
            btn_shift.text = getString(R.string.start_shift)
    }

    private fun handleError() {
        Log.d(TAG, "Error while getting shifts.")
    }

    private fun isShiftStarted() = btn_shift.text == getString(R.string.end_shift)
}