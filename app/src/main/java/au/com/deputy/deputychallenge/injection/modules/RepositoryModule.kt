package au.com.deputy.deputychallenge.injection.modules;

import au.com.deputy.deputychallenge.data.managers.ShiftsManager
import au.com.deputy.deputychallenge.data.repositories.ShiftsRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface RepositoryModule {
    @Binds
    @Singleton
    fun bindShiftsRepository(shiftsManager: ShiftsManager): ShiftsRepository
}