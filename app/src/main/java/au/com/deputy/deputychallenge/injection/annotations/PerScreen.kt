package au.com.deputy.deputychallenge.injection.annotations

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention(kotlin.annotation.AnnotationRetention.RUNTIME)
annotation class PerScreen
