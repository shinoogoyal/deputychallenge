package au.com.deputy.deputychallenge.data.api.entities

class ShiftDetails(val time: String,
                   val latitude:String,
                   val longitude: String)