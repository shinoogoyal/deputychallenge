package au.com.deputy.deputychallenge.data.api.entities

class PreviousShiftListDetails(val shifts: List<PreviousShift>)

class PreviousShift(val id: String,
                    val start: String,
                    val end: String,
                    val startLatitude: String,
                    val endLatitude: String,
                    val startLongitude: String,
                    val endLongitude: String,
                    val image: String)