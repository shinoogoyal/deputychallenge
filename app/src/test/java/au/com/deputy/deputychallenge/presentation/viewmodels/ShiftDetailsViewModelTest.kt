package au.com.deputy.deputychallenge.presentation.viewmodels

import android.arch.core.executor.testing.InstantTaskExecutorRule
import au.com.deputy.deputychallenge.data.repositories.ShiftsRepository
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import kotlin.test.assertEquals


@RunWith(org.mockito.junit.MockitoJUnitRunner::class)
class ShiftDetailsViewModelTest {

    @Rule
    @JvmField
    var instantExecutorRule = InstantTaskExecutorRule()

    private val shiftsRepository: ShiftsRepository = Mockito.mock(ShiftsRepository::class.java)
    private val viewmodel = ShiftDetailsViewModel()

    @Before
    fun setUp() {
        viewmodel.shiftsRepository = shiftsRepository
    }

    @Test
    fun getShifts_shiftRepository_getShiftsInvoked() {
        viewmodel.getShiftsList()
        Mockito.verify(shiftsRepository, Mockito.times(1)).getShifts(viewmodel)
    }

    @Test
    fun onShiftToggleResult_matches_shiftToggleState() {
        viewmodel.onShiftToggleResult(false)
        assertEquals(false, viewmodel.shiftToggleState.value)
    }

    @Test
    fun onShiftLoadedFailure_setErrorState() {
        viewmodel.onShiftsLoadedFailure()
        assertEquals(true, viewmodel.errorData.value is Exception)
    }

}