package au.com.deputy.deputychallenge.data.api.managers

import au.com.deputy.deputychallenge.data.api.entities.ShiftListResponse
import au.com.deputy.deputychallenge.data.api.services.ShiftsService
import au.com.deputy.deputychallenge.data.managers.ShiftsManager
import au.com.deputy.deputychallenge.data.repositories.ShiftsHandlerCallback
import au.com.deputy.deputychallenge.data.repositories.ShiftsRepository
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


@RunWith(org.mockito.junit.MockitoJUnitRunner::class)
class ShiftsManagerTest {

    private val shiftsService: ShiftsService = mock()
    private val callback: ShiftsHandlerCallback = mock()
    private val shiftsManager: ShiftsRepository = ShiftsManager(shiftsService)

    @Test
    fun getShifts_apiReturnsFailure_OnShiftsLoadedFailureCalled() {
        val remoteCall: Call<List<ShiftListResponse>?> = mock()
        Mockito.`when`(remoteCall.enqueue(any())).thenAnswer {
            it.getArgument<Callback<List<ShiftListResponse>?>>(0).onFailure(remoteCall, Throwable(Exception()))
        }

        Mockito.`when`(shiftsService.getShifts()).thenReturn(remoteCall)
        shiftsManager.getShifts(callback)
        Mockito.verify(callback, Mockito.times(1)).onShiftsLoadedFailure()
    }

    @Test
    fun getShifts_apiSuccessfulReturnsNull_OnShiftsLoaded() {
        val remoteCall: Call<List<ShiftListResponse>?> = mock()
        Mockito.`when`(remoteCall.enqueue(any())).thenAnswer {
            it.getArgument<Callback<List<ShiftListResponse>?>>(0).onResponse(remoteCall, Response.success(null))
        }
        Mockito.`when`(shiftsService.getShifts()).thenReturn(remoteCall)
        shiftsManager.getShifts(callback)

        Mockito.verify(callback, Mockito.times(1)).onShiftsLoaded(any())
    }

    @Test
    fun getShifts_apiFailure_OnShiftsLoadedFailure() {
        val remoteCall: Call<List<ShiftListResponse>?> = mock()
        Mockito.`when`(remoteCall.enqueue(any())).thenAnswer {
            it.getArgument<Callback<List<ShiftListResponse>?>>(0).onResponse(remoteCall, Response.error(400, ResponseBody.create(MediaType.parse("application/json"), "")))
        }
        Mockito.`when`(shiftsService.getShifts()).thenReturn(remoteCall)
        shiftsManager.getShifts(callback)
        Mockito.verify(callback, Mockito.times(1)).onShiftsLoadedFailure()
    }

    @Test
    fun getShifts_apiSuccess_OnShiftsLoaded() {
        val remoteCall: Call<List<ShiftListResponse>?> = mock()
        val response = "[{\"id\":1,\"start\":\"1543641803306\",\"end\":\"1543668227720\",\"startLatitude\":\"0.00\",\"startLongitude\":\"0.00\",\"endLatitude\":\"0.00\",\"endLongitude\":\"0.00\",\"image\":\"https://unsplash.it/500/500?random\"}]"
        Mockito.`when`(remoteCall.enqueue(any())).thenAnswer {
            val listType = object : TypeToken<List<ShiftListResponse>>() {}.type
            it.getArgument<Callback<List<ShiftListResponse>?>>(0).onResponse(remoteCall, Response.success(Gson().fromJson(response, listType)))
        }
        Mockito.`when`(shiftsService.getShifts()).thenReturn(remoteCall)
        shiftsManager.getShifts(callback)
        Mockito.verify(callback, Mockito.times(1)).onShiftsLoaded(any())
    }

}